const mongoose = require('mongoose');

const mongoDB = process.env.MONGODB_URI || 'mongodb://127.0.0.1/flashcardsDev';

mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = Promise;

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

module.exports = {mongoose};
