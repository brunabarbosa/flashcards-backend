const express = require("express");
const jwt = require("express-jwt");
const jwksRsa = require("jwks-rsa");

const flashcardController = require("./flashcardController");

const router = express.Router();

const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://brunabarbosa.auth0.com/.well-known/jwks.json`
  }),

  // Validate the audience and the issuer.
  audience: "m7Iwf3Z96yje8tZVvGVAGaey8zPDlbUA",
  issuer: `https://brunabarbosa.auth0.com/`,
  algorithms: ["RS256"]
});

router.get("/", flashcardController.getFlashcards);
router.get("/:id", flashcardController.getFlashcardById);
router.post("/", checkJwt, flashcardController.createFlashcard);
router.patch("/:id", flashcardController.updateFlashcard);
router.delete("/:id", flashcardController.deleteFlashcard);

module.exports = router;
