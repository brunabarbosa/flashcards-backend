const mongoose = require("mongoose");

const flashacardSchema = new mongoose.Schema({
  title: {
    type: String,
    trim: true,
    required: true
  },
  text: {
    type: String
  }
});

const Flashcard = mongoose.model("Flashcard", flashacardSchema);

module.exports = Flashcard;
