const {mongoose} = require('../config/databaseConfig');
const Flashcard = require('./flashcardModel');

exports.getFlashcards = async () => {
    const query = await Flashcard.find({});
    return query;
};

exports.getFlashcardById = async (id) => {
    const query = await Flashcard.findById({_id: id});
    return query;
};

exports.createFlashcard = async (flashcard) => {
    const query = await Flashcard.create(flashcard);
    return query;
};

exports.updateFlashcard = async (id, body) => {
    const query = await Flashcard.findOneAndUpdate({_id: id}, { $set: body }, { new: true });
    return query;
};

exports.deleteFlashcard = async (id) => {
    const query = await Flashcard.findOneAndRemove({_id: id});
    return query;
};