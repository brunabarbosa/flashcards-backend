const _ = require("lodash");
const mongoose = require("mongoose");
const uniqid = require("uniqid");

const Flashcard = require("./flashcardModel");
const flashcardRepository = require("./flashcardRepository");

const BAD_REQUEST = 400;
const RESOURCE_NOT_FOUND = 404;

exports.getFlashcards = async (req, res) => {
  try {
    const flashcardsReturned = await flashcardRepository.getFlashcards();
    res.send(flashcardsReturned);
  } catch (error) {
    res.status(BAD_REQUEST).send(error);
  }
};

exports.getFlashcardById = async (req, res) => {
  const id = req.params.id;

  try {
    const flashcardReturned = await flashcardRepository.getFlashcardById(id);
    if (!flashcardReturned) {
      return res.status(RESOURCE_NOT_FOUND).send();
    }
    res.send(flashcardReturned);
  } catch (error) {
    res.status(BAD_REQUEST).send(error);
  }
};

exports.createFlashcard = async (req, res) => {
  const flashcard = new Flashcard({
    title: req.body.title,
    text: req.body.text
  });

  try {
    const flashcardReturned = await flashcardRepository.createFlashcard(
      flashcard
    );
    res.send(flashcardReturned);
  } catch (error) {
    res.status(BAD_REQUEST).send(error);
  }
};

exports.updateFlashcard = async (req, res) => {
  const id = req.params.id;
  const body = _.pick(req.body, ["title", "text"]);

  try {
    const flashcardReturned = await flashcardRepository.updateFlashcard(
      id,
      body
    );
    if (!flashcardReturned) {
      return res.status(RESOURCE_NOT_FOUND).send();
    }
    res.send(flashcardReturned);
  } catch (error) {
    res.status(BAD_REQUEST).send(error);
  }
};

exports.deleteFlashcard = async (req, res) => {
  const id = req.params.id;

  try {
    const flashcardReturned = await flashcardRepository.deleteFlashcard(id);
    if (!flashcardReturned) {
      return res.status(RESOURCE_NOT_FOUND).send();
    }
    res.send(flashcardReturned);
  } catch (error) {
    res.status(BAD_REQUEST).send(error);
  }
};
