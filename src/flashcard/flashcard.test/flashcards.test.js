const expect = require("expect");
const request = require("supertest");

const app = require("../../app");
const Flashcard = require("../flashcardModel");

beforeEach(done => {
  Flashcard.remove({}).then(() => done());
});

describe("POST /flashcards", () => {
  it("should not create a new flashcard", done => {
    const title = "flashcard title";
    const text = "flashcard text";

    request(app)
      .post("/flashcards")
      .send({
        title,
        text
      })
      .expect(401)
      .end(done);
  });

  it("should not create flashcards w/out title", done => {
    request(app)
      .post("/flashcards")
      .send({})
      .expect(401)
      .end(done);
  });
});
