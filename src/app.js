const flashcardRouter = require("./flashcard/flashcardRouter");

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet");
const morgan = require("morgan");

const PORT = process.env.PORT || 5000;

const app = express();

app.use(helmet());
app.use(bodyParser.json());
app.use(cors());
app.use(morgan("combined"));

app.use("/flashcards", flashcardRouter);

app.listen(PORT, () => console.log(`Listing on port ${PORT}`));

module.exports = app;
