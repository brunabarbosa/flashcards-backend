## Bem vindo ao repositorio do projeto flashcards-backend!

## Projeto Flashcards
Quem nunca estudou de ultima hora para uma prova e esqueceu completamente do assunto estudado na noite
anterior? O projeto flashcards ajuda pessoas a aprender qualquer conteudo permanentemente e de forma 
mais eficiente. Neste App voce pode manter seus estudos organizados atraves de flashcards.

## Install
`npm install`

## Run
`npm start`

## Test
`npm test`

## Rotas implementadas - Ambiente Local
`GET http://localhost:5000/flashcards`   
`GET http://localhost:5000/flashcards/:id`   
`POST http://localhost:5000/flashcards              #Requer usuario autenticado para realizar POST`   
`PATCH http://localhost:5000/flashcards/:id`   
`DELETE http://localhost:5000/flashcards/:id`

## Rotas implementadas - Ambiente Heroku
`GET https://protected-gorge-19021.herokuapp.com/flashcards`   
`GET https://protected-gorge-19021.herokuapp.com/flashcards/:id`   
`POST https://protected-gorge-19021.herokuapp.com/flashcards              #Requer usuario autenticado para realizar POST`   
`PATCH https://protected-gorge-19021.herokuapp.com/flashcards  `   
`DELETE https://protected-gorge-19021.herokuapp.com/flashcards`

Para rodar esse projeto é necessário ter o Node Package Manager instalado e uma instancia do Mongo na porta 27017, para testar o servidor localmente.

O codigo do frontend esta disponivel [AQUI](https://github.com/brunabarbosa/flashcards-frontend). 
