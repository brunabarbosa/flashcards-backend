module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "indent": [4, "tab"],
        "linebreak-style": 0,
        "eol-last": 0
    }
};